#import "XMapsFlutterPlugin.h"
#if __has_include(<x_maps_flutter/x_maps_flutter-Swift.h>)
#import <x_maps_flutter/x_maps_flutter-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "x_maps_flutter-Swift.h"
#endif

@implementation XMapsFlutterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftXMapsFlutterPlugin registerWithRegistrar:registrar];
}
@end
