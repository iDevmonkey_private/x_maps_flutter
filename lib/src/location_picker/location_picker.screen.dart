import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import '../../x_maps_flutter.dart';
import 'location_picker.widget.dart';
import 'models.dart';

export 'models.dart';

class LocationPickers extends StatefulWidget {
  static Future<Poi> show(BuildContext context, {LatLng currentLatLng}) {
    return Navigator.push<Poi>(
      context,
      MaterialPageRoute<Poi>(
        builder: (BuildContext context) => LocationPickers(currentLatLng: currentLatLng,),
      ),
    );
  }

  LocationPickers({Key key, this.currentLatLng}) : super(key: key);
  final LatLng currentLatLng;

  @override
  _LocationPickersState createState() => _LocationPickersState();
}

class _LocationPickersState extends State<LocationPickers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LocationPicker(
        requestPermission: () {
          return Permission.location.request().then((it) => it.isGranted);
        },
        poiItemBuilder: (poi, selected) {
          return ListTile(
            title: Text(poi.name),
            subtitle: Text(poi.address),
            trailing: selected ? Icon(Icons.check) : SizedBox.shrink(),
          );
        },
        onItemSelected: (data) {
          Navigator.pop(context, data?.poi);
        },
        currentLatLng: widget.currentLatLng,
      ),
    );
  }
}
