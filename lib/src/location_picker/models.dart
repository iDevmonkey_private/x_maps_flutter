import '../../x_maps_flutter.dart';

class PoiInfo {
  PoiInfo(this.poi);

  final Poi poi;
  bool selected = false;

  @override
  String toString() {
    return 'PoiInfo{poi: $poi, selected: $selected}';
  }
}

/// 兴趣点 model
class Poi {
  Poi({
    this.address,
    this.name,
    this.latLng,
    this.cityName,
    this.cityCode,
    this.provinceName,
    this.provinceCode,
    this.tel,
    this.poiId,
    this.businessArea,
    this.distance,
    this.adName,
    this.adCode,
  });

  /// 地址
  String address;

  ///标题
  String name;

  /// 经纬度
  LatLng latLng;

  /// 城市名
  String cityName;

  /// 城市编码
  String cityCode;

  /// 省份名称
  String provinceName;

  /// 省份编码
  String provinceCode;

  /// 电话
  String tel;

  /// 兴趣点id
  String poiId;

  /// 商业区
  String businessArea;

  /// 距离
  int distance;

  /// 行政区划名称
  String adName;

  /// 行政区划编号
  String adCode;

  //
  Map map;

  static Poi fromMap(Map map) {
    if (map == null) return null;
    return Poi()..fromJson(map);
  }

  void fromJson(Map map) {
    this.map = map;
    this.poiId = map["id"];
    this.name = map["name"];
    this.address = map["address"];

    this.provinceCode = map["pcode"];
    this.provinceName = map["pname"];
    this.cityCode = map["citycode"];
    this.cityName = map["cityname"];
    this.adCode = map["adcode"];
    this.adName = map["adname"];
    this.tel = map["tel"];
    this.businessArea = map["business_area"];
    this.distance = int.tryParse(map["distance"] as String);

    String locStr = map["location"];
    if (locStr?.isNotEmpty ?? false) {
      List<String> l = locStr.split(",");
      if (l?.length == 2) {
        this.latLng = LatLng(double.tryParse(l[1]), double.tryParse(l[0]));
      }
    }
  }
}

